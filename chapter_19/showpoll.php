<?php
    /****************************************
     Database query to get poll info
     ****************************************/
     // log into database
     if(!$db_conn = @mysql_connect("localhost", "poll", "poll"))
     {
         echo "Could not connect to the db<br>";
         exit;
     }
     @mysql_select_db("poll");

     if(!empty($vote))                              // If they filled the form, add their vote
     {
         $vote = addslashes($vote);
         $query = "update poll_results set num_votes = num_votes + 1 where candidate = '$vote'";
         if(!($result = @mysql_query($query, $db_conn)))
         {
             echo "Could not connect to the db<br>";
             exit;
         }
     }

     // get current results of poll, regardless of whether they voted
     $query = "select * from poll_results";
     if(!($result = @mysql_query(($query, $db_conn))))
     {
        echo "Could not connect to the db<br>";
        exit;
     }
     $num_candidates = mysql_num_rows($result);

     // calculate the total number of votes so far
     $total_votes = 0;
     while($row = mysql_fetch_object($result))
     {
        $total_votes += $row->num_votes;
     }
     mysql_data_seek($result, 0);                   // reset the result pointer

     /*************************************************
       Initial calculations for graph
      *************************************************/
    // set up constants
       $width = 500;                                // width of the image in pixels
       $left_margin = 50;                           // space to leave on left of image
       $right_margin = 50;                          // ditto right
       $bar_height = 40;
       $bar_spacing = $bar_height / 2;
       $font = "arial.ttf";
       $title_size = 16;                            // point
       $main_size = 12;                             // point
       $small_size = 12;                            // point
       $text_indent = 10;                           // position for text labels on left

       // set up the initial point to draw from
       $x = $left_margin + 60;                      // place to draw baseline on graph
       $y = 50;                                     // ditto
       $bar_unit = ($width - ($x + $right_margin)) / 100;   // one 'point' on the graph

       // calculate the height of the graph
       $height = $num_candidates * ($bar_height + $bar_spacing) + 50;

       /************************************************
         Set up base image
        ************************************************/
        // create a blank canvas
       $im = imagecreate($width, $height);
       
       // Allocate colours
       $white = imagecolorallocate($im, 255, 255, 255);
       $blue = imagecolorallocate($im, 0, 64, 128);
       $black = imagecolorallocate($im, 0, 0, 0);
       $pink = imagecolorallocate($im, 255, 78, 243);
       $text_color = $black;
       $percent_color = $black;
       $bg_color = $white;
       $line_color = $black;
       $bar_color = $blue;
       $number_color = $pink;

       // Create a canvas to draw on
       imagefilledrectangle($im, 0, 0, $width, $height, $bg_color);

       // Draw outline around said canvas
       imagerectangle($im, 0, 0, $width - 1, $height - 1, $line_color);

       // Add title
       $title = "Poll Results";
       $title_dimensions = imagettfbbox($title_size, 0, $font, $title);
       $title_length = $title_dimensions[2] - $title_dimensions[0];
       $title_height = abs(title_dimensions[7] - $title_dimensions[1]);
       $title_above_line = abs($title_dimensions[7]);
       $title_x = ($width - $title_length) / 2;     // centre on x
       $title_y = ($y - $title_height) / 2 + $title_above_line;         // centre in y gap
       imagettftext($im, $title_size, 0, $title_x, $title_y, $text_color, $font, $title);

       // Draw a base line from a little above the first bar location
       // to a little below the last
       imageline($im, $x, $y - 5, $x, $height - 15, $line_color);

       /**********************************************
         Draw data into the graph
        **********************************************/
       // Get each line of db data and draw corresponding bars
       while($row = mysql_fetch_object($result))
       {
            if(total_votes) > 0
                $percent = intval(round(($row->num_votes / $total_votes) * 100));
            else
                $percent = 0;

            // display the percent for this value
            imagettftext($im, $main_size, 0, $width - 30, $y + ($bar_height / 2), $percent_color, $font, $percent."%");

            if(total_votes > 0)
                $right_value = intval(round(($row->num_votes / $total_votes) * 100));
            else
                $right_value = 0;

            // length of bar for this value
            $bar_length = $x + ($right_value * $bar_unit);

            // draw bar for this value
            imagefilledrectangle($im, $x, $y - 2, $bar_length, $y + bar_height, $bar_color);

            // draw a title for this value
            imagettftext($im, $main_size, 0, $text_indent, $y + ($bar_height / 2), $text_color, $font, "$row->candidate");

            // draw outline showing 100%
            imagerectangle($im, $bar_length + 1, $y - 2, ($x + (100 * bar_unit)), $y + bar_height, $line_color);

            // display numbers
            imagettftext($im, $small_size, 0, $x + (100 * bar_unit) - 50, $y + ($bar_height / 2), $number_color, $font, $row->num_votes."/".total_votes);

            // move down to the next bar
            $y = $y + ($bar_height + $bar_spacing);
       }  

       /***********************************************
         Display image
        ***********************************************/
       Header("Content-type: image/png");
       imagepng($im);

       /***********************************************
         Clean up
        ***********************************************/
       imagedestroy($im);  
?>
